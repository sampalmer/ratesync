# Ratesync

Android TV app to automatically switch display refresh rate to match content frame rate for smoother video playback

## Current status

Working with Netflix only. This project is abandoned and needs a new developer to complete it.

## Local development

Prerequisites:
* Android Studio
* NVIDIA Shield TV since this won't run on an emulator

To run this app locally:
1. Make sure your Shield is on the same network as your development machine
1. In your Shield's developer settings, turn on Network Debugging
1. Note your Shield's IP address
1. On your development machine, run `adb connect x.x.x.x` where `x.x.x.x` is your Shield's IP address

After doing the above, you can build and install the app on your Shield just like normal.

## Architecture

Ratesync runs two separate processes:
| Process | Purpose | Usage |
| ------- | ------- | ----- |
| UI | UI code. Can be killed by OS when UI is not visible. | For all UI code. Allowed to use plenty of RAM for images and icons etc as needed. |
| Service | "Always on" background code. Designed to stay alive as long as possible and minimise risk of being killed by OS. | Must use low amount of RAM so OS doesn't kill process. Must not run UI code since that uses a lot of RAM. |

Having two processes adds some complexity but provides the following benefits:
* Background service won't die if UI crashes
* Less chances of background service being killed by OS during memory pressure, since the OS kills memory hogs first
* Leaves more RAM free for other apps since the OS can readily kill the UI process

Most multi-process complexity is already taken care of in the codebase. Main thing is to follow the directions in the comments in `ApplicationSettings.java`.

## Persistent data

Ratesync stores persistent data in a few different places:
|  Name  | Location | Data | Reason |
| ------ | -------- | ------- | ------ |
| Application settings | `app.xml` | User preferences | Standard practice |
| Service settings | `service.xml` | Persistent service state and cached copies of user preferences | Service might need to save data across process or device restarts, and Android [`SharedPreferences`](https://developer.android.com/reference/android/content/SharedPreferences) is not safe to share between processes |
| ADB keys | [Android keystore](https://developer.android.com/training/articles/keystore) | RSA key-pair | To authenticate against the device's ADB server without storing the private key in plain-text |

## Development notes

There are lots of development notes in the Wiki
