package samuelpalmer.ratesync.processes.service;

import android.content.Context;
import android.util.Log;

import samuelpalmer.ratesync.AllPermissionsMonitor;
import samuelpalmer.ratesync.processes.ui.DisableAppReceiver;
import samuelpalmer.ratesync.utilities.State;
import samuelpalmer.ratesync.utilities.eventhandling.EventHandler;

/**
 * Disables the app when a required permission is revoked. Note that permanent ADB authorisation is
 * not covered since that cannot be monitored.
 */
public class AppDisabler extends State {

    private static final String TAG = AppDisabler.class.getSimpleName();

    private final AllPermissionsMonitor allPermissionsMonitor;
    private final Context context;

    public AppDisabler(Context context) {
        allPermissionsMonitor = new AllPermissionsMonitor(context);
        this.context = context;
    }

    @Override
    protected void start() {
        allPermissionsMonitor.subscribe(allPermissionsChanged);
        check();
    }

    @Override
    protected void stop() {
        allPermissionsMonitor.unsubscribe(allPermissionsChanged);
    }

    private final EventHandler allPermissionsChanged = this::check;

    private void check() {
        boolean hasAllPermissions = allPermissionsMonitor.hasPermissions();
        if (!hasAllPermissions) {
            Log.w(TAG, "Not all permissions are granted. Disabling app.");
            DisableAppReceiver.sendBroadcast(context);
        }
    }


}
