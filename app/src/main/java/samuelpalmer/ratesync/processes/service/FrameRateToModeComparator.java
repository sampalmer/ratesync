package samuelpalmer.ratesync.processes.service;

import android.view.Display;

import java.util.Comparator;

/**
 * Same as {@link FrameRateToRefreshRateComparator} but takes {@link android.view.Display.Mode}s
 * and compares their refresh rates.
 */
public class FrameRateToModeComparator implements Comparator<Display.Mode> {

    private final FrameRateToRefreshRateComparator delegate;

    public FrameRateToModeComparator(float frameRateFps) {
        delegate = new FrameRateToRefreshRateComparator(frameRateFps);
    }

    @Override
    public int compare(Display.Mode mode1, Display.Mode mode2) {
        return delegate.compare(mode1.getRefreshRate(), mode2.getRefreshRate());
    }

}
