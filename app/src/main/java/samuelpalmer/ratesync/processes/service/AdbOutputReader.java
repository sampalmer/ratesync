package samuelpalmer.ratesync.processes.service;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.MainThread;
import androidx.core.os.HandlerCompat;

import com.tananaev.adblib.AdbConnection;
import com.tananaev.adblib.AdbStream;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import samuelpalmer.ratesync.adb.AdbBackgroundConnection;
import samuelpalmer.ratesync.processes.ui.DisableAppReceiver;
import samuelpalmer.ratesync.utilities.State;

/**
 * Runs an ADB shell command and sends back each line of response text continuously. The shell
 * command is expected to run forever rather than returning back to the caller.
 */
public class AdbOutputReader extends State {

    private static final String TAG = AdbOutputReader.class.getSimpleName();

    public interface Listener {
        /**
         * Called on the main thread after a complete line of text arrives.
         * Will never be called after a call to {@link #ensureStopped()}.
         */
        @MainThread
        void onLine(String line);
    }

    private final Handler handler;
    private final Context context;
    private final String shellCommand;
    private final Listener listener;
    private final AdbBackgroundConnection backgroundConnection;

    @MainThread
    public AdbOutputReader(Context context, String shellCommand, Listener listener) {
        this.context = context;
        this.shellCommand = shellCommand;
        this.listener = listener;
        handler = HandlerCompat.createAsync(Looper.getMainLooper());
        backgroundConnection = new AdbBackgroundConnection(connectionListener);
    }

    @MainThread
    @Override
    protected void start() {
        backgroundConnection.ensureStarted();
    }

    @MainThread
    @Override
    protected void stop() {
        backgroundConnection.ensureStopped();
    }

    @SuppressWarnings("FieldCanBeLocal")
    private final AdbBackgroundConnection.ConnectionCallbacks connectionListener = new AdbBackgroundConnection.ConnectionCallbacks() {
        @Override
        public void onConnected(AdbConnection connection) throws InterruptedException {
            // Send ADB command
            try (AdbStream stream = connection.open("shell:" + shellCommand)) {
                // Start processing response data
                StringBuilder buffer = new StringBuilder();

                while (!Thread.interrupted()) {
                    // Reading the next chunk of data
                    byte[] chunkBytes;
                    chunkBytes = stream.read();

                    // Decoding chunk
                    String chunkText = new String(chunkBytes, StandardCharsets.UTF_8);
                    buffer.append(chunkText);

                    // Splitting chunk into lines
                    while (true) {
                        int lineBreakIndex = buffer.indexOf("\n");
                        if (lineBreakIndex < 0)
                            break;

                        String line = buffer.substring(0, lineBreakIndex);

                        // We're still in the worker thread, so we need to post the result back to the main thread
                        handler.post(() -> {
                            if (isStarted())
                                listener.onLine(line);
                        });

                        buffer.delete(0, lineBreakIndex + 1);
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException(String.format("Error running ADB command `%s`", shellCommand), e);
            }
        }

        @Override
        public void onPermanentAuthMissing() {
            Log.w(TAG, "ADB permanent auth is missing. Disabling app.");
            DisableAppReceiver.sendBroadcast(context);
        }

        @Override
        public void onNetworkDebuggingDisabled() {
            // This is a permission issue that will be handled at a higher level in the app code.
            // We'll just ignore it here.
            Log.w(TAG, "Connection failed because network debugging is disabed. Ignoring.");
        }
    };

}
