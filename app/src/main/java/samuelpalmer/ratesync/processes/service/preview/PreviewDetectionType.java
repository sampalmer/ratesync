package samuelpalmer.ratesync.processes.service.preview;

import android.content.Context;
import android.media.session.MediaController;

@SuppressWarnings({"unused", "RedundantSuppression"})
public enum PreviewDetectionType {
    NONE((context, session) -> new NullPreviewDetector()),
    PAUSE(PausePreviewDetector::new);

    private final Factory factory;

    PreviewDetectionType(Factory factory) {
        this.factory = factory;
    }

    public PreviewDetector createDetector(Context context, MediaController session) {
        return factory.create(context, session);
    }

    private interface Factory {
        PreviewDetector create(Context context, MediaController session);
    }
}
