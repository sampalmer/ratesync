package samuelpalmer.ratesync.processes.service;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.media.session.MediaController;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.ServiceCompat;

import samuelpalmer.ratesync.R;
import samuelpalmer.ratesync.processes.service.preview.PreviewDetector;
import samuelpalmer.ratesync.profile.AppProfile;
import samuelpalmer.ratesync.profile.AppProfiles;
import samuelpalmer.ratesync.utilities.eventhandling.EventHandler;

/**
 * Keeps the app process alive and coordinates background logic
 */
public class AutoRefreshRateService extends Service {

    private static final String TAG = AutoRefreshRateService.class.getSimpleName();

    private FrameRateMonitor frameRateMonitor;
    private RefreshRateOverrider refreshRateOverrider;
    private PlaybackMonitor playbackMonitor;
    private AppDisabler appDisabler;

    private Float frameRate;
    private MediaController currentSession;
    private PreviewDetector previewDetector;
    private MediaRefreshRateSwitcher rateSwitcher;

    private boolean isPossiblyPreview = true;
    private State state;

    private enum State {
        /** Not doing anything. Waiting for the user to start a media session. */
        IDLE,

        /** A media session is active. Detecting the frame rate and preview status. */
        DETECTING,

        /** Switching the refresh rate for the current media. */
        SWITCHING,

        /** In the new refresh rate for the current media. */
        SWITCHED,

        /** Not running */
        DESTROYED,
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i(TAG, "Created");
        state = State.IDLE;

        // Starting foreground mode to ensure the app process is not killed by the OS
        Notification serviceNotification = new NotificationCompat.Builder(this, Notifications.CHANNEL_SERVICE)
                .setContentTitle(getText(R.string.app_name))
                .setSmallIcon(R.drawable.ic_notification)
                .build();

        startForeground(Notifications.ID_SERVICE, serviceNotification);

        frameRateMonitor = new FrameRateMonitor(this);
        refreshRateOverrider = new RefreshRateOverrider(this);
        playbackMonitor = new PlaybackMonitor(this);
        appDisabler = new AppDisabler(this);

        frameRateMonitor.subscribe(gotFrameRate);
        refreshRateOverrider.ensureStarted();
        playbackMonitor.subscribe(mediaSessionChanged);
        appDisabler.ensureStarted();

        // We might already be in a media session, so we'll take a look straight away
        mediaSessionChanged.update();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "Destroyed");

        if (rateSwitcher != null) {
            rateSwitcher.unsubscribe(rateSwitchStateChanged);
            rateSwitcher = null;
        }

        resetSession();

        state = State.DESTROYED;

        appDisabler.ensureStopped();
        playbackMonitor.unsubscribe(mediaSessionChanged);
        refreshRateOverrider.ensureStopped();
        frameRateMonitor.unsubscribe(gotFrameRate);

        appDisabler = null;
        playbackMonitor = null;
        refreshRateOverrider = null;
        frameRateMonitor = null;

        ServiceCompat.stopForeground(this, ServiceCompat.STOP_FOREGROUND_REMOVE);
    }

    private final EventHandler mediaSessionChanged = new EventHandler() {
        @Override
        public void update() {
            MediaController newSession = playbackMonitor.getCurrentSession();

            switch (state) {
                case IDLE:
                    if (newSession == null) {
                        if (currentSession != null) {
                            Log.i(TAG, "Session ended");
                            resetSession();
                        }

                        return;
                    }

                    Log.i(TAG, "Got media session. Starting detection.");
                    startDetection(newSession);

                    break;
                case DETECTING:
                    if (currentSession == null)
                        throw new IllegalStateException("In state " + State.DETECTING.name() + " but don't have a session");

                    if (newSession == null) {
                        Log.i(TAG, "Media session ended. Resetting.");
                        resetSession();
                    } else {
                        Log.i(TAG, "Media session changed. Restarting detection.");
                        resetSession();
                        startDetection(newSession);
                    }

                    break;
                case SWITCHING:
                    // Don't care about session changes here. That's the responsibility of the switcher class!
                    currentSession = newSession;
                    break;
                case SWITCHED:
                    if (newSession == null) {
                        Log.i(TAG, "Session ended. Restoring default refresh rate.");
                        refreshRateOverrider.updateRefreshRateForFrameRate(null);
                        resetSession();
                    } else if (currentSession == null) {
                        Log.i(TAG, "Got current session.");
                        currentSession = newSession;
                    } else {
                        Log.i(TAG, "Session changed. Restoring default refresh rate. Restarting detection.");
                        refreshRateOverrider.updateRefreshRateForFrameRate(null);
                        resetSession();
                        startDetection(newSession);
                    }

                    break;
                case DESTROYED:
                    throw new IllegalStateException("Received media session change while destroyed");
            }
        }
    };

    private void startDetection(MediaController newSession) {
        if (state != State.IDLE)
            throw new IllegalStateException("Got start detection, but in state " + state.name() + " instead of " + State.IDLE.name());

        if (newSession == null)
            throw new IllegalArgumentException("New session must not be null");

        currentSession = newSession;

        AppProfile appProfile = AppProfiles.getProfileForPackage(newSession.getPackageName());
        if (appProfile == null) {
            Log.i(TAG, "Got session for unsupported package: " + newSession.getPackageName() + ". Ignoring.");
            return;
        }

        state = State.DETECTING;
        previewDetector = appProfile.getPreviewType().createDetector(AutoRefreshRateService.this, newSession);
        previewDetector.subscribe(previewStatusChanged);
        isPossiblyPreview = previewDetector.isPossiblyPreview();
        previewStatusChanged.update(); // Just in case detection is instantaneous
    }

    private void resetSession() {
        frameRate = null;
        isPossiblyPreview = true;

        if (currentSession != null)
            currentSession = null;

        if (previewDetector != null) {
            previewDetector.unsubscribe(previewStatusChanged);
            previewDetector = null;
        }

        state = State.IDLE;
    }

    private final EventHandler gotFrameRate = new EventHandler() {
        @SuppressWarnings("RedundantSuppression")
        @Override
        public void update() {
            //noinspection ConstantConditions
            float newFrameRate = frameRateMonitor.getFrameRate();
            if (frameRate != null) {
                Log.w(TAG, "Got frame rate " + newFrameRate + " FPS, but we already have frame rate " + frameRate + ". Ignoring.");
                return;
            }

            frameRate = newFrameRate;

            // We get this notification just *after* the session starts in Netflix, but just *before* it starts in YouTube
            Log.i(TAG, "Got frame rate " + AutoRefreshRateService.this.frameRate + " Hz");
            somethingChanged();
        }
    };

    private final EventHandler previewStatusChanged = new EventHandler() {
        @Override
        public void update() {
            if (state != State.DETECTING)
                throw new IllegalStateException("Preview status changed, but we're not in " + State.DETECTING.name() + " state");

            if (previewDetector.isPossiblyPreview()) {
                // We currently don't care if the media session turns back into a preview. That's not a supported scenario yet. :)
                return;
            }

            Log.i(TAG, "Playback is not a preview");
            isPossiblyPreview = false;
            previewDetector.unsubscribe(previewStatusChanged);
            previewDetector = null;
            somethingChanged();
        }
    };

    private void somethingChanged() {
        if (frameRate != null && !isPossiblyPreview) {
            Log.i(TAG, "Got frame rate and normal playback. Switching refresh rate.");
            state = State.SWITCHING;
            rateSwitcher = new MediaRefreshRateSwitcher(this, playbackMonitor, refreshRateOverrider, frameRate);
            rateSwitcher.subscribe(rateSwitchStateChanged);
        }
    }

    private final EventHandler rateSwitchStateChanged = new EventHandler() {
        @Override
        public void update() {
            if (state != State.SWITCHING)
                throw new IllegalStateException("Got rate switch state change, but in state " + state.name() + " instead of " + State.SWITCHING.name());

            switch (rateSwitcher.getState()) {
                case NOT_STARTED:
                    Log.i(TAG, "Refresh rate switching aborted. Resetting.");

                    rateSwitcher.unsubscribe(rateSwitchStateChanged);
                    rateSwitcher = null;

                    state = State.IDLE;
                    currentSession = null;

                    if (playbackMonitor.getCurrentSession() != null)
                        mediaSessionChanged.update();

                    break;
                case COMPLETE:
                    Log.i(TAG, "Finished switching");

                    rateSwitcher.unsubscribe(rateSwitchStateChanged);
                    rateSwitcher = null;

                    currentSession = playbackMonitor.getCurrentSession();
                    // The session may or may not be null, depending on the app

                    state = State.SWITCHED;
                    break;
                default:
                    // Switching is still in progress, so we don't care
                    break;
            }
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Start command received");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.w(TAG, "Device memory is low. Process might be killed.");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
