package samuelpalmer.ratesync.processes.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import samuelpalmer.ratesync.processes.ui.SettingsSynchroniser;

/**
 * Receives {@link samuelpalmer.ratesync.processes.ui.ApplicationSettings} from the UI process and stores them in {@link ServiceSettings}
 * so that the service process can use them
 */
public class SettingsReceiver extends BroadcastReceiver {

    private static final String TAG = SettingsReceiver.class.getSimpleName();
    private static final String EXTRA_SETTINGS = "settings";

    /**
     * Sends the given app settings to the service process
     *
     * @param appSettings The app settings from {@link samuelpalmer.ratesync.processes.ui.ApplicationSettings} to send to synchronise to {@link ServiceSettings}
     */
    public static void sendSettings(Context context, Map<String, Object> appSettings) {
        Intent intent = new Intent(context, SettingsReceiver.class);
        Bundle settingsBundle = prefsToBundle(appSettings);
        intent.putExtra(EXTRA_SETTINGS, settingsBundle);
        context.sendBroadcast(intent);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "Received settings");

        Bundle settingsBundle = intent.getBundleExtra(EXTRA_SETTINGS);
        SharedPreferences prefs = ServiceSettings.getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();

        // It's possible that a setting was removed from the UI process, so we'll replace the full
        // set of syncable settings with the new lot that we received
        clearSyncableAppSettings(prefs, editor);
        bundleToPrefs(settingsBundle, editor);

        editor.apply();
    }

    private void clearSyncableAppSettings(SharedPreferences prefs, SharedPreferences.Editor editor) {
        Set<String> keys = prefs.getAll().keySet();

        for (String key : keys)
            if (SettingsSynchroniser.isSyncableSetting(key))
                editor.remove(key);
    }

    private static Bundle prefsToBundle(Map<String, Object> prefs) {
        Bundle bundle = new Bundle();

        for (String key : prefs.keySet()) {
            Object value = prefs.get(key);
            if (value == null)
                bundle.putString(key, null);
            else if (value instanceof String)
                bundle.putString(key, (String) value);
            else if (value instanceof Boolean)
                bundle.putBoolean(key, (boolean) value);
            else if (value instanceof Integer)
                bundle.putInt(key, (int) value);
            else if (value instanceof Float)
                bundle.putFloat(key, (float) value);
            else if (value instanceof Long)
                bundle.putLong(key, (long) value);
            else if (value instanceof Set<?>) {
                Set<?> set = (Set<?>) value;
                if (!set.isEmpty()) {
                    Object setItem = set.iterator().next();
                    if (!(setItem instanceof String)) {
                        Class<?> setItemType = setItem == null ? null : setItem.getClass();
                        throw new RuntimeException(String.format("Set setting with key '%s' has unexpected item '%s' of type %s but expected set of strings", key, "" + setItem, setItemType));
                    }
                }
                //noinspection unchecked
                Set<String> stringSet = (Set<String>) set;
                String[] stringArray = stringSet.toArray(new String[0]);
                bundle.putStringArray(key, stringArray);
            }
            else
                throw new RuntimeException(String.format("Setting with key '%s' has unexpected value '%s' of type %s", key, value.toString(), value.getClass().toString()));
        }

        return bundle;
    }

    private void bundleToPrefs(Bundle bundle, SharedPreferences.Editor editor) {
        for (String key : bundle.keySet()) {
            Object value = bundle.get(key);
            if (value == null)
                editor.remove(key);
            else if (value instanceof String)
                editor.putString(key, (String) value);
            else if (value instanceof Boolean)
                editor.putBoolean(key, (boolean) value);
            else if (value instanceof Integer)
                editor.putInt(key, (int) value);
            else if (value instanceof Float)
                editor.putFloat(key, (float) value);
            else if (value instanceof Long)
                editor.putLong(key, (long) value);
            else if (value instanceof String[]) {
                String[] stringArray = (String[]) value;
                editor.putStringSet(key, new HashSet<>(Arrays.asList(stringArray)));
            } else
                throw new RuntimeException(String.format("Received unexpected value '%s' of type %s for key '%s'", value.toString(), value.getClass().toString(), key));
        }
    }

}
