package samuelpalmer.ratesync.processes.service;

import android.content.ComponentName;
import android.content.Context;
import android.media.MediaDescription;
import android.media.MediaMetadata;
import android.media.session.MediaController;
import android.media.session.MediaSession;
import android.media.session.MediaSessionManager;
import android.media.session.PlaybackState;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.os.HandlerCompat;

import java.util.List;

import samuelpalmer.ratesync.utilities.FieldConstants;
import samuelpalmer.ratesync.utilities.ObjectExtensions;
import samuelpalmer.ratesync.utilities.eventhandling.EventRegistrarAdaptor;

public class PlaybackMonitor extends EventRegistrarAdaptor {

    private static final String TAG = PlaybackMonitor.class.getSimpleName();

    private final MediaSessionManager mediaSessionManager;
    private final Handler handler;
    private final ComponentName notificationListener;

    private boolean isUpToDate = false;
    private MediaController currentSession;

    public PlaybackMonitor(Context context) {
        mediaSessionManager = ContextCompat.getSystemService(context, MediaSessionManager.class);
        handler = HandlerCompat.createAsync(Looper.getMainLooper());
        notificationListener = new ComponentName(context, NotificationListener.class);
    }

    @Nullable
    public MediaController getCurrentSession() {
        if (isUpToDate)
            return currentSession;
        else {
            List<MediaController> controllers;

            try {
                controllers = mediaSessionManager.getActiveSessions(notificationListener);
            } catch (SecurityException e) {
                // The notification listener is not enabled, so we won't be able to do anything. We'll
                // just ignore the error and proceed since permissions are handled at a higher level in
                // the app.
                Log.w(TAG, "Notification listener is not enabled. Could not get media sessions.", e);
                return null;
            }

            return controllers.isEmpty() ? null : controllers.get(0);
        }
    }

    @Override
    protected void subscribeToAdaptee() {
        try {
            mediaSessionManager.addOnActiveSessionsChangedListener(sessionsChangedListener, notificationListener, handler);
        } catch (SecurityException e) {
            // The notification listener is not enabled, so we won't be able to do anything. We'll
            // just ignore the error and proceed since permissions are handled at a higher level in
            // the app.
            Log.w(TAG, "Notification listener is not enabled. Could not add media session listener.", e);
        }

        currentSession = null;
    }

    @Override
    protected void unsubscribeFromAdaptee() {
        mediaSessionManager.removeOnActiveSessionsChangedListener(sessionsChangedListener);
        isUpToDate = false;
    }
    
    private final MediaSessionManager.OnActiveSessionsChangedListener sessionsChangedListener = new MediaSessionManager.OnActiveSessionsChangedListener() {
        @Override
        public void onActiveSessionsChanged(@Nullable List<MediaController> controllers) {
            if (!hasSubscriptions())
                return;

            Log.i(TAG, "Active media sessions changed. Count = " + (controllers == null ? 0 : controllers.size()));

            if (controllers != null) {
                for (MediaController controller : controllers) {
                    StringBuilder dump = new StringBuilder();

                    dump.append("{");

                    dump.append("metadata=");

                    @Nullable MediaMetadata metadata = controller.getMetadata();
                    if (metadata == null) {
                        dump.append("null");
                    } else {
                        dump.append("{");

                        MediaDescription mediaDescription = metadata.getDescription();
                        dump.append("description={");

                        CharSequence title = mediaDescription.getTitle();
                        CharSequence subtitle = mediaDescription.getSubtitle();
                        CharSequence description = mediaDescription.getDescription();
                        String mediaId = mediaDescription.getMediaId();
                        Uri mediaUri = mediaDescription.getMediaUri();

                        dump.append("title=");
                        dump.append(title);
                        dump.append(", ");

                        dump.append("subtitle=");
                        dump.append(subtitle);
                        dump.append(", ");

                        dump.append("description=");
                        dump.append(description);
                        dump.append(", ");

                        dump.append("mediaId=");
                        dump.append(mediaId);
                        dump.append(", ");

                        dump.append("mediaUri=");
                        dump.append(mediaUri);
                        dump.append("}");

                        dump.append("}");
                    }
                    dump.append(", ");

                    dump.append("extras=");
                    dump.append(controller.getExtras());
                    dump.append(", ");

                    long flags = controller.getFlags();
                    //noinspection deprecation
                    boolean handlesMediaButtons = (flags & MediaSession.FLAG_HANDLES_MEDIA_BUTTONS) != 0;
                    //noinspection deprecation
                    boolean handlesTransportControls = (flags & MediaSession.FLAG_HANDLES_TRANSPORT_CONTROLS) != 0;

                    dump.append("handlesMediaButtons=");
                    dump.append(handlesMediaButtons);
                    dump.append(", ");

                    dump.append("handlesTransportControls=");
                    dump.append(handlesTransportControls);
                    dump.append(", ");

                    String packageName = controller.getPackageName();
                    dump.append("packageName=");
                    dump.append(packageName);
                    dump.append(", ");

                    PlaybackState playbackState = controller.getPlaybackState();
                    dump.append("playbackState=");
                    dump.append(playbackState == null ? "null" : formatPlaybackState(playbackState));
                    dump.append(", ");

                    MediaSession.Token sessionToken = controller.getSessionToken();
                    dump.append("sessionToken=");
                    dump.append(sessionToken);
                    dump.append("}");

                    Log.i(TAG, "Media session: " + dump);

                    controller.registerCallback(new CustomMediaSessionCallback(controller), handler);
                }

                // We only support one active session at this stage
                MediaController newSession = controllers.isEmpty() ? null : controllers.get(0);
                MediaSession.Token currentSessionToken = currentSession == null ? null : currentSession.getSessionToken();
                MediaSession.Token newSessionToken = newSession == null ? null : newSession.getSessionToken();

                // Sometimes the MediaController instance changes but has the same token and
                // corresponds to the same session, so won't report that as a change
                if (!ObjectExtensions.areEqual(currentSessionToken, newSessionToken)) {
                    Log.i(TAG, "Session changed to " + newSessionToken);
                    currentSession = newSession;
                    isUpToDate = true;
                    report();
                }
            }
        }
    };

    private static String formatPlaybackState(PlaybackState playbackState) {
        StringBuilder result = new StringBuilder();

        long actions = playbackState.getActions();
        boolean canPause = (actions & PlaybackState.ACTION_PAUSE) != 0;
        boolean canPlay = (actions & PlaybackState.ACTION_PLAY) != 0;
        boolean canStop = (actions & PlaybackState.ACTION_STOP) != 0;
        boolean canFastForward = (actions & PlaybackState.ACTION_FAST_FORWARD) != 0;
        boolean canRewind = (actions & PlaybackState.ACTION_REWIND) != 0;
        boolean canSeek = (actions & PlaybackState.ACTION_SEEK_TO) != 0;
        boolean canSkipToNext = (actions & PlaybackState.ACTION_SKIP_TO_NEXT) != 0;
        boolean canSkipToPrevious = (actions & PlaybackState.ACTION_SKIP_TO_PREVIOUS) != 0;
        boolean canSkipToQueueItem = (actions & PlaybackState.ACTION_SKIP_TO_QUEUE_ITEM) != 0;


        result.append("{");

        result.append("canPause=");
        result.append(canPause);
        result.append(", ");

        result.append("canPlay=");
        result.append(canPlay);
        result.append(", ");

        result.append("canStop=");
        result.append(canStop);
        result.append(", ");

        result.append("canFastForward=");
        result.append(canFastForward);
        result.append(", ");

        result.append("canRewind=");
        result.append(canRewind);
        result.append(", ");

        result.append("canSeek=");
        result.append(canSeek);
        result.append(", ");

        result.append("canSkipToNext=");
        result.append(canSkipToNext);
        result.append(", ");

        result.append("canSkipToPrevious=");
        result.append(canSkipToPrevious);
        result.append(", ");

        result.append("canSkipToQueueItem=");
        result.append(canSkipToQueueItem);
        result.append(", ");

        result.append("customActions=[");

        List<PlaybackState.CustomAction> customActions = playbackState.getCustomActions();
        for (PlaybackState.CustomAction customAction : customActions) {
            result.append("{");

            String action = customAction.getAction();
            Bundle actionExtras = customAction.getExtras();
            CharSequence name = customAction.getName();

            result.append("action=");
            result.append(action);
            result.append(", ");

            result.append("extras=");
            result.append(actionExtras);
            result.append(", ");

            result.append("name=");
            result.append(name);

            result.append("}");
        }

        result.append("], ");

        result.append("extras=");
        result.append(playbackState.getExtras());
        result.append(", ");

        int state = playbackState.getState();
        String stateName = new FieldConstants<>(PlaybackState.class, "STATE_").getName(state);

        result.append("state=");
        result.append(stateName);
        result.append(", ");

        result.append("position=");
        result.append(playbackState.getPosition());
        result.append(", ");

        result.append("lastPositionUpdateTime=");
        result.append(playbackState.getLastPositionUpdateTime());
        result.append(", ");

        result.append("playbackSpeed=");
        result.append(playbackState.getPlaybackSpeed());
        result.append(", ");

        result.append("errorMessage=");
        result.append(playbackState.getErrorMessage());
        result.append("}");

        return result.toString();
    }

    private class CustomMediaSessionCallback extends MediaController.Callback {

        private final MediaController controller;

        public CustomMediaSessionCallback(MediaController controller) {
            this.controller = controller;
        }
        
        @Override
        public void onSessionDestroyed() {
            super.onSessionDestroyed();
            
            if (!hasSubscriptions())
                return;
            
            Log.i(TAG, "Session destroyed: " + controller.getSessionToken());
        }

        @Override
        public void onSessionEvent(@NonNull String event, @Nullable Bundle extras) {
            super.onSessionEvent(event, extras);

            if (!hasSubscriptions())
                return;

            Log.i(TAG, "Session event: token = " + controller.getSessionToken() + ", event = " + event + ", extras = " + extras);
        }

        @Override
        public void onPlaybackStateChanged(@Nullable PlaybackState state) {
            super.onPlaybackStateChanged(state);

            if (!hasSubscriptions())
                return;

            Log.v(TAG, "Session playback state changed: " + (state == null ? "null" : formatPlaybackState(state)));
        }

        @Override
        public void onMetadataChanged(@Nullable MediaMetadata metadata) {
            super.onMetadataChanged(metadata);

            if (!hasSubscriptions())
                return;

            Log.i(TAG, "Session metadata changed: " + metadata);
        }

        @Override
        public void onExtrasChanged(@Nullable Bundle extras) {
            super.onExtrasChanged(extras);

            if (!hasSubscriptions())
                return;

            Log.i(TAG, "Session extras changed: " + extras);
        }
        
    }

}
