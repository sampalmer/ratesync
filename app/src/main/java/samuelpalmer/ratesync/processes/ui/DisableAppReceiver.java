package samuelpalmer.ratesync.processes.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Allows the service process to disable the app. The app can only be directly disabled from the UI
 * process, so this provides a way for the service process to ask the UI process to do so.
 */
public class DisableAppReceiver extends BroadcastReceiver {

    private static final String TAG = DisableAppReceiver.class.getSimpleName();

    /**
     * Sends a broadcast to the UI process to have the app disabled
     */
    public static void sendBroadcast(Context context) {
        context.sendBroadcast(new Intent(context, DisableAppReceiver.class));
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "Received request to disable app");
        ensureAppIsDisabled(context);
    }

    /**
     * Disables the app if it is currently enabled
     */
    private static void ensureAppIsDisabled(Context context) {
        SharedPreferences prefs = ApplicationSettings.getSharedPreferences(context);

        boolean isEnabled = prefs.getBoolean(ApplicationSettings.KEY_ENABLED, false);
        if (!isEnabled) {
            // App is already disabled, so nothing to do
            return;
        }

        prefs
                .edit()
                .putBoolean(ApplicationSettings.KEY_ENABLED, false)
                .apply();
    }

}
