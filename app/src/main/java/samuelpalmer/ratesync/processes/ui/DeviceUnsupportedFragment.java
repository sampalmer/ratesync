package samuelpalmer.ratesync.processes.ui;

import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.leanback.app.ErrorSupportFragment;

import samuelpalmer.ratesync.R;

/**
 * Informs the user that this app won't work on their device because it's not an NVIDIA Shield TV
 */
public class DeviceUnsupportedFragment extends ErrorSupportFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getResources().getString(R.string.app_name));

        Drawable errorIcon = ResourcesCompat.getDrawable(getResources(), androidx.leanback.R.drawable.lb_ic_sad_cloud, null);
        setImageDrawable(errorIcon);

        CharSequence message = getString(R.string.device_unsupported_explanation, getString(R.string.app_name));
        setMessage(message);

        setDefaultBackground(false);

        setButtonText(getString(R.string.device_unsupported_close));
        setButtonClickListener(v -> requireActivity().finish());
    }

}
