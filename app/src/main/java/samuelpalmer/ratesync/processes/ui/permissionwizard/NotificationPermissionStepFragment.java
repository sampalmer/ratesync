package samuelpalmer.ratesync.processes.ui.permissionwizard;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.leanback.app.GuidedStepSupportFragment;

import samuelpalmer.ratesync.processes.service.NotificationListener;
import samuelpalmer.ratesync.R;
import samuelpalmer.ratesync.utilities.NotificationListenerEnabledStateMonitor;
import samuelpalmer.ratesync.utilities.eventhandling.EventRegistrar;

/**
 * Prompts the user to grant the special "Notification access" permission to this app
 */
public class NotificationPermissionStepFragment extends PermissionStepFragment {

    private NotificationListenerEnabledStateMonitor permissionMonitor;

    @Override
    protected int getTitleResourceId() {
        return R.string.permission_notification_title;
    }

    @Override
    protected int getExplanationResourceId() {
        return R.string.permission_notification_explanation;
    }

    @Override
    protected EventRegistrar onPermissionChanged() {
        return permissionMonitor;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        permissionMonitor = new NotificationListenerEnabledStateMonitor(context, NotificationListener.class);
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        permissionMonitor = null;
    }

    @Override
    protected boolean hasPermission() {
        return permissionMonitor.isEnabled();
    }

    @Override
    protected Intent getPromptIntent() {
        return NotificationListener.makeAcquirePermissionIntent();
    }

    @Override
    protected CharSequence getInstructions() {
        return getString(R.string.permission_notification_instructions, getString(R.string.app_name));
    }

    @Override
    protected GuidedStepSupportFragment getNextStep() {
        return new DisplayOverOtherAppsPermissionStepFragment();
    }

}
