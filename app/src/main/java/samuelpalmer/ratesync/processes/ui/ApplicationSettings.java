package samuelpalmer.ratesync.processes.ui;

import android.content.Context;
import android.content.SharedPreferences;

import samuelpalmer.ratesync.CurrentProcess;

/**
 * The app's main {@link SharedPreferences} file. For use by the UI process only.
 * This is intentionally kept simple so that {@link SharedPreferences} access is consistent with
 * other apps.
 */
@SuppressWarnings("DanglingJavadoc")
public abstract class ApplicationSettings {

    public static SharedPreferences getSharedPreferences(Context context) {
        if (CurrentProcess.isServiceProcess(context))
            throw new RuntimeException("Application settings accessed outside of the UI process");

        return context.getSharedPreferences(getSharedPreferencesName(context), Context.MODE_PRIVATE);
    }

    @SuppressWarnings("SameReturnValue")
    public static String getSharedPreferencesName(Context context) {
        if (CurrentProcess.isServiceProcess(context))
            throw new RuntimeException("Application settings accessed outside of the UI process");

        return "app";
    }

    /*
     * NOTE: If you change preference config:
     * 1. Add a migration to the "Migrations" class
     * 2. If you're altering a user preference, ensure the changes are synced to the service process
     */

    /** Used for upgrading settings data when upgrading to a newer version of the app */
    public static final String KEY_SETTINGS_VERSION = "version";

    /**
     * Whether the app is in "on" state. This setting is the source of truth about what the target
     * service state is.
     */
    public static final String KEY_ENABLED = "isEnabled";

    /**
     * If you add a preference that the service needs to access, then add a check in
     * {@link SettingsSynchroniser#isSyncableSetting(String)} to whitelist the setting
     * for synchronisation to the service process
     */
}
