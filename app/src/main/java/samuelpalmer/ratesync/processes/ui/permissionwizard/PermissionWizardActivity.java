package samuelpalmer.ratesync.processes.ui.permissionwizard;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.leanback.app.GuidedStepSupportFragment;

/**
 * Prompts the user to grant all permissions required by this app
 */
public class PermissionWizardActivity extends FragmentActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            GuidedStepSupportFragment fragment = new UsagePermissionStepFragment();
            GuidedStepSupportFragment.addAsRoot(this, fragment, android.R.id.content);
        }
    }

    @Override
    public void onBackPressed() {
        // After the user has granted a permission, we don't want them to be able to go back to the
        // permission request screen since it doesn't make sense
        finish();
    }

}
