package samuelpalmer.ratesync.processes.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.core.content.ContextCompat;

import samuelpalmer.ratesync.processes.service.AutoRefreshRateService;

/**
 * Convenience class to start and stop the main service
 */
public class ServiceController {

    private static final String TAG = ServiceController.class.getSimpleName();

    /**
     * Updates the service state to match the state in {@link ApplicationSettings#KEY_ENABLED}
     */
    public static void syncServiceState(Context context) {
        SharedPreferences prefs = ApplicationSettings.getSharedPreferences(context);
        Intent service = new Intent(context, AutoRefreshRateService.class);
        boolean isEnabled = prefs.getBoolean(ApplicationSettings.KEY_ENABLED, false);

        if (isEnabled) {
            Log.i(TAG, "Ensuring service is started");
            ContextCompat.startForegroundService(context, service);
        } else {
            Log.i(TAG, "Ensuring service is stopped");
            context.stopService(service);
        }
    }

    private ServiceController() {}

}
