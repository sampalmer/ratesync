package samuelpalmer.ratesync.profile;

import androidx.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;

import samuelpalmer.ratesync.processes.service.preview.PreviewDetectionType;

/**
 * The source of truth for which profile to use for each media app
 */
public class AppProfiles {
    private static final Map<String, AppProfile> profilesByPackage;

    static {
        profilesByPackage = new HashMap<>();
        profilesByPackage.put("com.netflix.ninja", new AppProfile(PreviewDetectionType.PAUSE));
//        profilesByPackage.put("com.google.android.youtube.tv", new AppProfile(PreviewDetectionType.NONE));
    }

    @Nullable
    public static AppProfile getProfileForPackage(String packageName) {
        return profilesByPackage.get(packageName);
    }

    private AppProfiles(){}
}
