package samuelpalmer.ratesync.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.util.SparseArray;

import samuelpalmer.ratesync.CurrentProcess;
import samuelpalmer.ratesync.processes.service.ServiceSettings;
import samuelpalmer.ratesync.processes.ui.ApplicationSettings;

/**
 * Handles differences in app setting structure across different versions of the app
 */
public class Upgrader {

    private static final int CURRENT_VERSION = 1;

    private static final String TAG = Upgrader.class.getSimpleName();

    /**
     * Updates app settings to the format and structure used in the current running version of the
     * app
     */
    public static void upgradeAppSettings(Context context) {
        SharedPreferences prefs = getPrefsForProcess(context);

        int lastVersion = prefs.getInt(getLastVersionKey(context), 0);
        if (lastVersion == CURRENT_VERSION) {
            Log.d(TAG, "No upgrade needed");
            // Settings version hasn't changed, so we won't do anything
            return;
        }

        if (lastVersion == 0) {
            // This is the first time the app has started up, so we'll record the current version
            // for use when the app is upgraded in the future
            Log.i(TAG, "App started for first time. Recording settings version.");
        } else {
            if (lastVersion > CURRENT_VERSION) {
                // The app has been downgraded. We don't know the settings format for the future
                // version of the app, so we'll have to start from scratch.
                Log.i(TAG, String.format("App downgraded from settings version %d to version %d. Clearing settings.", lastVersion, CURRENT_VERSION));
                prefs.edit().clear().apply();
            } else {
                // The settings need to be upgraded, so we'll run the upgrade script for each
                // version between the old version and the new version
                Log.i(TAG, String.format("Upgrading from settings version %d to version %d", lastVersion, CURRENT_VERSION));
                runMigrations(context, prefs, lastVersion + 1, CURRENT_VERSION);
            }
        }

        prefs
                .edit()
                .putInt(getLastVersionKey(context), CURRENT_VERSION)
                .apply();
    }

    /**
     * Gets the {@link SharedPreferences} key for the version number that's currently saved with
     * the settings
     */
    private static String getLastVersionKey(Context context) {
        if (CurrentProcess.isServiceProcess(context))
            return ServiceSettings.KEY_SETTINGS_VERSION;
        else
            return ApplicationSettings.KEY_SETTINGS_VERSION;
    }

    private static SharedPreferences getPrefsForProcess(Context context) {
        if (CurrentProcess.isServiceProcess(context))
            return ServiceSettings.getSharedPreferences(context);
        else
            return ApplicationSettings.getSharedPreferences(context);
    }

    private static void runMigrations(Context context, SharedPreferences prefs, int fromVersion, @SuppressWarnings("SameParameterValue") int toVersion) {
        SparseArray<Migration> migrations = Migrations.buildMap();

        for (int version = fromVersion; version <= toVersion; ++version) {
            Migration migration = migrations.get(version);
            if (migration != null) {
                Log.i(TAG, "Running upgrade script for version " + version);

                SharedPreferences.Editor editor = prefs.edit();

                if (CurrentProcess.isServiceProcess(context))
                    migration.upgradeServiceProcessData(context, prefs, editor);
                else
                    migration.upgradeUiProcessData(context, prefs, editor);

                // Keeping the settings version in sync with the upgrade scripts in case the app crashes or is killed part-way through.
                editor.putInt(getLastVersionKey(context), version);
                editor.apply();
            }
        }
    }

    private Upgrader() {}

}
