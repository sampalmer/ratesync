package samuelpalmer.ratesync.utilities;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.MainThread;
import androidx.core.os.HandlerCompat;

import com.tananaev.adblib.AdbConnection;

import samuelpalmer.ratesync.adb.AdbBackgroundConnection;
import samuelpalmer.ratesync.utilities.eventhandling.UpdatingEventRegistrar;

/**
 * Determines whether the user has given this app permanent permission to use ADB.
 * Unfortunately, the only way to check this is to make an ADB connection, which
 * can make a permission popup appear.
 *
 * Permission is only considered granted when the user has checked the
 * "Always allow from this computer" box in the permission dialog.
 */
public class AdbPermissionCheck extends State {

    private static final String TAG = AdbPermissionCheck.class.getSimpleName();

    public final UpdatingEventRegistrar onResult = new UpdatingEventRegistrar();

    private final Handler handler;
    private final AdbBackgroundConnection adb;

    private boolean gotResult;
    private boolean isPermanentlyAuthorised;

    @MainThread
    public AdbPermissionCheck() {
        handler = HandlerCompat.createAsync(Looper.getMainLooper());
        adb = new AdbBackgroundConnection(connectionCallbacks);
    }

    /** Whether the user has granted ADB permission with the "Always allow from this computer" box checked */
    public boolean isPermanentlyAuthorised() {
        if (!isStarted())
            throw new IllegalStateException("Cannot check result before we get it");

        return isPermanentlyAuthorised;
    }

    @MainThread
    @Override
    protected void start() {
        gotResult = false;
        isPermanentlyAuthorised = false;
        adb.ensureStarted();
    }

    @MainThread
    @Override
    protected void stop() {
        adb.ensureStopped();
        handler.removeCallbacks(connectionSuccessul);
    }

    @SuppressWarnings("FieldCanBeLocal")
    private final AdbBackgroundConnection.ConnectionCallbacks connectionCallbacks = new AdbBackgroundConnection.ConnectionCallbacks() {
        @Override
        public void onConnected(AdbConnection connection) {
            handler.post(connectionSuccessul);
        }

        @Override
        public void onPermanentAuthMissing() {
            setIsPermanentlyAuthorised(false);
        }

        @Override
        public void onNetworkDebuggingDisabled() {
            setIsPermanentlyAuthorised(false);
        }
    };

    @SuppressWarnings("Convert2Lambda")
    private final Runnable connectionSuccessul = new Runnable() {
        @MainThread
        @Override
        public void run() {
            setIsPermanentlyAuthorised(true);
        }
    };

    @MainThread
    private void setIsPermanentlyAuthorised(boolean result) {
        if (!isStarted()) {
            Log.w(TAG, "Setting connection result to " + result + " when not started. Ignoring.");
            return;
        }

        if (gotResult) {
            Log.d(TAG, "Setting connection result to " + result + " when we already have a result. Ignoring.");
            return;
        }

        isPermanentlyAuthorised = result;
        gotResult = true;
        onResult.report();
    }

}
