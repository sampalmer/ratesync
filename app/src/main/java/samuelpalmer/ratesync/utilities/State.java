package samuelpalmer.ratesync.utilities;

/**
 * Provides common logic and a single convention for classes that can be "started" and "stopped".
 */
public abstract class State {

    private boolean started;

    /**
     * Starts if we aren't already started
     */
    public void ensureStarted() {
        if (!started) {
            start();
            started = true;
        }
    }

    /**
     * Stops if we aren't already stopped
     */
    public void ensureStopped() {
        if (started) {
            stop();
            started = false;
        }
    }

    @SuppressWarnings({"BooleanMethodIsAlwaysInverted", "RedundantSuppression"})
    public boolean isStarted() {
        return started;
    }

    /**
     * Throws an exception if {@link #isStarted()} is {@code false}
     */
    protected void verifyStarted() {
        if (!isStarted())
            throw new IllegalStateException("Not started");
    }

    protected abstract void start();

    protected abstract void stop();

}
