package samuelpalmer.ratesync.utilities;

import android.annotation.SuppressLint;
import android.os.Build;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Helper class to get details about the particular device that this app is running on
 */
public class DeviceInfo {

    /**
     * Checks whether the device is an official Android emulator
     */
    public static boolean isEmulator() {
        // We only want to detect the official Android emulator since that's the one with
        // the special device orientation controls that we can use.
        //
        // Apparently some devices may contain this property but have it set to <c>0</c>, so it's
        // not enough to just check for the property's presence: https://stackoverflow.com/q/46643273/238753
        String qemuValue = getSystemProperty("ro.kernel.qemu");
        return qemuValue != null && qemuValue.equals("1");
    }

    /**
     * Checks whether the device is an NVIDIA Shield TV.
     * This is needed because other Android TV devices don't make video frame rates available.
     */
    public static boolean isNvidiaShieldTv() {
        return "NVIDIA".equals(Build.BRAND) && "SHIELD Android TV".equals(Build.MODEL);
    }

    private static String getSystemProperty(@SuppressWarnings("SameParameterValue") String name) {
        try {
            @SuppressLint("PrivateApi")
            Class<?> systemPropertyClazz = Class.forName("android.os.SystemProperties");
            Method getSystemProperty = systemPropertyClazz.getMethod("get", String.class);
            return (String) getSystemProperty.invoke(systemPropertyClazz, name);
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    private DeviceInfo() {}

}
