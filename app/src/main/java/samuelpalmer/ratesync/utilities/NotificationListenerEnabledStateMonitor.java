package samuelpalmer.ratesync.utilities;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.os.Looper;
import android.provider.Settings;
import android.service.notification.NotificationListenerService;
import android.text.TextUtils;

import androidx.core.os.HandlerCompat;

import samuelpalmer.ratesync.utilities.eventhandling.EventRegistrarAdaptor;

public class NotificationListenerEnabledStateMonitor extends EventRegistrarAdaptor {

    @SuppressWarnings({"unused", "RedundantSuppression"})
    private static final String TAG = NotificationListenerEnabledStateMonitor.class.getSimpleName();
    private static final String SETTING_ENABLED_NOTIFICATION_LISTENERS = "enabled_notification_listeners";

    private final static TextUtils.SimpleStringSplitter colonSplitter = new TextUtils.SimpleStringSplitter(':');
    private final ComponentName targetService;
    private final ContentResolver contentResolver;

    private boolean isUpToDate = false;
    private boolean wasEnabled;

    public NotificationListenerEnabledStateMonitor(Context context, Class<? extends NotificationListenerService> notificationListener) {
        targetService = new ComponentName(context, notificationListener);
        contentResolver = context.getContentResolver();
    }

    public boolean isEnabled() {
        if (isUpToDate)
            return wasEnabled;
        else
            return poll();
    }

    @Override
    protected void subscribeToAdaptee() {
        contentResolver.registerContentObserver(Settings.Secure.getUriFor(SETTING_ENABLED_NOTIFICATION_LISTENERS), false, observer);
    }

    @Override
    protected void unsubscribeFromAdaptee() {
        contentResolver.unregisterContentObserver(observer);
        isUpToDate = false;
    }

    private final ContentObserver observer = new ContentObserver(HandlerCompat.createAsync(Looper.getMainLooper())) {
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);

            if (hasSubscriptions()) {
                boolean isEnabled = poll();
                if (!isUpToDate || isEnabled != wasEnabled) {
                    wasEnabled = isEnabled;
                    isUpToDate = true;
                    report();
                }
            }
        }
    };

    /**
     * Implementation mostly copied from the AccessibilityUtils Android class
     */
    private boolean poll() {
        String enabledServicesSetting = Settings.Secure.getString(contentResolver, SETTING_ENABLED_NOTIFICATION_LISTENERS);
        if (enabledServicesSetting == null)
            return false;

        colonSplitter.setString(enabledServicesSetting);

        while (colonSplitter.hasNext()) {
            String componentNameString = colonSplitter.next();
            ComponentName enabledService = ComponentName.unflattenFromString(componentNameString);

            if (enabledService != null && enabledService.equals(targetService))
                return true;
        }

        return false;
    }

}
