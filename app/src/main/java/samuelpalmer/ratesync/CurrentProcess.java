package samuelpalmer.ratesync;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.os.Build;

import androidx.annotation.Nullable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Provides information about the process that the caller is running in
 */
public class CurrentProcess {

    @Nullable
    private static Boolean isServiceProcess;

    @Nullable
    private static String name;

    public static boolean isServiceProcess(Context context) {
        if (isServiceProcess == null) {
            String processName = getCurrentProcessName();

            String packageName = context.getPackageName();
            if (processName.equals(packageName + context.getText(R.string.process_service)))
                isServiceProcess = true;
            else if (processName.equals(packageName + context.getText(R.string.process_ui)))
                isServiceProcess = false;
            else
                throw new RuntimeException("Unrecognised process name: " + processName);
        }

        return isServiceProcess;
    }


    public static String getCurrentProcessName() {
        if (name == null) {
            if (Build.VERSION.SDK_INT >= 28)
                name = Application.getProcessName();
            else {
                // This does the same thing as Application.getProcessName() but for older OS versions

                //noinspection TryWithIdenticalCatches
                try {
                    @SuppressLint("PrivateApi")
                    Class<?> activityThread = Class.forName("android.app.ActivityThread");

                    // Before API 18, the method was incorrectly named "currentPackageName", but it still returned the process name
                    // See https://github.com/aosp-mirror/platform_frameworks_base/commit/b57a50bd16ce25db441da5c1b63d48721bb90687
                    @SuppressLint("ObsoleteSdkInt")
                    String methodName = Build.VERSION.SDK_INT >= 18 ? "currentProcessName" : "currentPackageName";

                    Method getProcessName = activityThread.getDeclaredMethod(methodName);
                    name = (String) getProcessName.invoke(null);
                } catch (ClassNotFoundException e) {
                    throw new RuntimeException(e);
                } catch (NoSuchMethodException e) {
                    throw new RuntimeException(e);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                } catch (InvocationTargetException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        return name;
    }

    private CurrentProcess() {}

}
