package samuelpalmer.shieldsimulator;

import androidx.annotation.NonNull;

import java.text.NumberFormat;

public class FrameRate {

    public static final FrameRate[] all = new FrameRate[] {
            new FrameRate(23.976f, 24),
            new FrameRate(24.000f, 24),
            new FrameRate(25.000f, 25),
            new FrameRate(29.970f, 30),
            new FrameRate(30.000f, 30),
            new FrameRate(50.000f, 50),
            new FrameRate(59.940f, 62),
            new FrameRate(60.000f, 62),
    };

    public final float actual;
    public final int nvidiaLogStartUp;

    private FrameRate(float actual, int nvidiaLogStartUp) {
        this.actual = actual;
        this.nvidiaLogStartUp = nvidiaLogStartUp;
    }

    @Override
    @NonNull
    public String toString() {
        return NumberFormat.getInstance().format(actual);
    }

}
